module.exports = {
	id: String,
	clientId: String,
	clientSecret: String,
	grants: [String],
	scope: [String],
	redirectUris: [String]
};
