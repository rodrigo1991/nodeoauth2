module.exports = {
	accessToken: String,
	accessTokenExpiresAt: Date,
	refreshToken: String,
	refreshTokenExpiresAt: Date,
	scope: [String],
	client: Object,
	user: Object,
	
};
